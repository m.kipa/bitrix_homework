<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->IncludeComponent(
	"linux.soft", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"ELEMENT_COUNT" => "4",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "4"
	),
	false
);

$APPLICATION->IncludeComponent(
        'bitrix:iblock.element.add.form', '',array(
        "SEF_MODE" => "Y",
        "IBLOCK_TYPE" => "news",
        "IBLOCK_ID" => "1",
        "PROPERTY_CODES" => array("NAME","IBLOCK_SECTION","DETAIL_TEXT","DETAIL_PICTURE"),
        "PROPERTY_CODES_REQUIRED" => array("NAME","IBLOCK_SECTION","DETAIL_PICTURE"),
        "GROUPS" => array("1"),
        "STATUS_NEW" => "2",
        "STATUS" => array("2"),
        "LIST_URL" => "",
        "ELEMENT_ASSOC" => "PROPERTY_ID",
        "ELEMENT_ASSOC_PROPERTY" => "",
        "MAX_USER_ENTRIES" => "100000",
        "MAX_LEVELS" => "100000",
        "LEVEL_LAST" => "Y",
        "USER_MESSAGE_EDIT" => "",
        "USER_MESSAGE_ADD" => "",
        "DEFAULT_INPUT_SIZE" => "30",
        "RESIZE_IMAGES" => "Y",
        "MAX_FILE_SIZE" => "0",
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
        "SEF_FOLDER" => "/",
        "VARIABLE_ALIASES" => Array(
        )
    )
);?>
</><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>