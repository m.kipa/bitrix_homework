<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Loader;

class LinuxSoft extends CBitrixComponent
{
    public function ExecuteComponent()
    {
        Loader::includeModule('iblock');
        $dbResult = CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            ['IBLOCK_ID' => 4],
            false,
            ['nTopCount' => 6],
            ['NAME', 'LAST_VERSION', 'IBLOCK_SECTION_ID', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'],
        );
        $elementSection = CIBlockSection::GetByID(
            18
        );
        $resultElementSection = $elementSection->GetNext();

        var_dump($resultElementSection);

        while($objectResult = $dbResult->GetNextElement()){
        $arFields = $objectResult->GetFields();
            $this->arResult[] = [
                'NAME' => $arFields['NAME'],
                'LAST_VERSION' => $arFields['LAST_VERSION'],
                'IBLOCK_SECTION_ID' => $resultElementSection['NAME'],
                'PREVIEW_PICTURE' => $arFields['PREVIEW_PICTURE'],
                'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
            ];
        }
        $this->IncludeComponentTemplate();
    }
}

//2 категории это две папки