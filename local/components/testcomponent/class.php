<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CDemoSqr extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 36000000,
            "X" => intval($arParams["X"]),
        );
        return $result;
    }

    public function sqr($x)
    {
        return $x * $x;
    }

    public function executeComponent()
    {
        if($this->startResultCache()) // кэширование arResult
        {
            $this->arResult["Y"] = $this->sqr($this->arParams["X"]);
            $this->includeComponentTemplate();
        }
    }
}?>